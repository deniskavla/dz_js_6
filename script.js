// Опишите своими словами как работает цикл forEach.
//forEach() проходит по массиву и выполняет callback возвращает этот же массив

function filterBy(arr, type) {
    return arr.filter(function (elem) {
        return (typeof(elem) !== type);
    });
};
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
